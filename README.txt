
Infos
 
Source:
https://github.com/mickaelbaron/javamicroservices-tutorial/tree/master/javamicroservices-tutorial-exercice7

Creer un réseau interne sous docker:
docker network create microservicesnetwork

Pour vérifier le réseau docker créé:
docker network ls

Pour supprimer un réseau docker:
docker network rm <NETWORK_ID>

Pour lancer docker-compose:
docker-compose up -d

Pour stopper et supprimer:
docker-compose down

Image docker rabbitmq:
https://hub.docker.com/_/rabbitmq?tab=tags&page=1&name=alp
https://hub.docker.com/_/rabbitmq?tab=description&page=1&name=alp
-> standard management port of 15672, with the default username and password of guest / guest: http://container-ip:15672

Image docker redis:
https://hub.docker.com/_/redis/?tab=tags&page=1&name=alpine
https://hub.docker.com/_/redis/?tab=description&page=1&name=alpine

SpringBoot - RabbitMQ : https://spring.io/guides/gs/messaging-rabbitmq/

